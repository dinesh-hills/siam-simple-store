# Simple API

### Packages
- DRF
- django-filter

### endpoints
- store/products
- store/category

### Supported query params
/products

#### Filters
- Category
- Brand (ilike)
- Price (range)

#### Sort
- rating
- price
- last_update

#### Search
- title

### Example query params
- ?category=1
- ?price__gt=10
- ?price__gt=10&price__lt=1000
- ordering=price (-value, for desc, eg. -price)